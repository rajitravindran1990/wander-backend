package com.wander.dashboard;

import com.wander.dashboard.beans.CovidDataHeader;
import com.wander.dashboard.beans.CovidDataRow;
import lombok.Data;

import java.util.List;

@Data
public class CovidData {
    List<CovidDataHeader> columns;
    List<CovidDataRow> rows;
}
