package com.wander.dashboard.beans;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CovidDataHeader {
    private String label;
    private String field;
    private String sort;
}
