package com.wander.dashboard.beans;

import lombok.Data;

@Data
public class CovidDataRow {
    private String country;
    private Object tested;
    private Object infected;
    private Object recovered;
    private Object deceased;
    private String lastUpdatedSource;
}
