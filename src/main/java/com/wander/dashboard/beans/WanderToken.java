package com.wander.dashboard.beans;


import lombok.Data;

@Data
public class WanderToken {
    private Long id;
    private String email;
    private boolean isAdmin;
}
