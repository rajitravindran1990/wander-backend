package com.wander.dashboard.beans.request;

import lombok.Data;

@Data
public class SignUpReq {
    private String email;
    private String name;
    private String password;
}
