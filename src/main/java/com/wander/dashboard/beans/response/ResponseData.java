package com.wander.dashboard.beans.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseData {
    String message;
    boolean success;
    int statusCode;
    Object body;
}
