package com.wander.dashboard.controller;

import com.wander.dashboard.beans.User;
import com.wander.dashboard.beans.request.LoginRequest;
import com.wander.dashboard.beans.request.SignUpReq;
import com.wander.dashboard.beans.response.ResponseData;
import com.wander.dashboard.exceptions.WanderException;
import com.wander.dashboard.service.AuthService;
import com.wander.dashboard.service.JwtService;
import com.wander.dashboard.utils.Constants;
import com.wander.dashboard.utils.ResourceUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthController extends BaseController {

    @Autowired
    private AuthService authService;

    @Autowired
    private JwtService jwtService;

    @PostMapping(ResourceUrl.SIGN_UP)
    ResponseEntity<ResponseData> signUp(@RequestBody SignUpReq signupData) {
        User user = null;
        try {
            user = authService.signUp(signupData);
            user.setPassword(null);
        } catch (WanderException e) {
            return failure(null, e.getStatusCode(), e.getMessage());
        }
        return success(user, HttpStatus.CREATED, "Success!!");
    }

    @PostMapping(ResourceUrl.LOGIN)
    ResponseEntity<ResponseData> login(@RequestBody LoginRequest loginRequest, HttpServletResponse response) {
        User user = null;
        try {
            user = authService.login(loginRequest);
            user.setPassword(null);
        } catch (WanderException e) {
            return failure(null, e.getStatusCode(), e.getMessage());
        }

        Cookie cookie = new Cookie(Constants.WANDER_TOKEN_HEADER, jwtService.getTokenForUser(user));
        cookie.setSecure(false);
        cookie.setHttpOnly(false);
        cookie.setPath("/");
        response.addCookie(cookie);

        return success(user, HttpStatus.OK, "Logged In!!");
    }
}
