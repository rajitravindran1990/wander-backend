package com.wander.dashboard.controller;

import com.wander.dashboard.beans.User;
import com.wander.dashboard.beans.response.ResponseData;
import com.wander.dashboard.service.JwtService;
import com.wander.dashboard.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

import javax.servlet.http.Cookie;

public class BaseController<T> {

    @Autowired
    private JwtService jwtService;

    public ResponseEntity<ResponseData> success(T t, HttpStatus status, @Nullable String message) {
        ResponseData resp = new ResponseData();
        resp.setBody(t);
        resp.setMessage(message);
        resp.setStatusCode(status.value());
        resp.setSuccess(true);
        return ResponseEntity.ok(resp);
    }

    public ResponseEntity<ResponseData> failure(T t, HttpStatus status, @Nullable String message) {
        ResponseData resp = new ResponseData();
        resp.setBody(t);
        resp.setMessage(message);
        resp.setStatusCode(status.value());
        resp.setSuccess(false);
        return ResponseEntity.ok(resp);
    }
}
