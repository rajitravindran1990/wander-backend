package com.wander.dashboard.controller;

import com.wander.dashboard.CovidData;
import com.wander.dashboard.beans.CovidDataRow;
import com.wander.dashboard.beans.response.ResponseData;
import com.wander.dashboard.exceptions.WanderException;
import com.wander.dashboard.service.DashboardService;
import com.wander.dashboard.service.JwtService;
import com.wander.dashboard.utils.Constants;
import com.wander.dashboard.utils.ResourceUrl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class DashboardController extends BaseController {

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private JwtService jwtService;

    @GetMapping(ResourceUrl.DASHBOARD)
    ResponseEntity<ResponseData> getCovidData(@RequestHeader(value = Constants.WANDER_TOKEN_HEADER) String token) {
        if (StringUtils.isBlank(token)) {
            return failure(null, HttpStatus.UNAUTHORIZED, "Please log in!!");
        }
        try {
            jwtService.decodeJwtToken(token);
        } catch (Exception e) {
            return failure(null, HttpStatus.UNAUTHORIZED, "Invalid token!!");
        }
        CovidData data = null;
        try {
            data = dashboardService.getCovidData();
        } catch (WanderException e) {
            return failure(null, e.getStatusCode(), e.getMessage());
        }
        return success(data, HttpStatus.OK, "Success!!");
    }
}