package com.wander.dashboard.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class AuthException extends WanderException {

    public AuthException(String message, HttpStatus statusCode) {
        super(message, statusCode);
    }
}
