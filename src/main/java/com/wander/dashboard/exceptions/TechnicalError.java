package com.wander.dashboard.exceptions;

import org.springframework.http.HttpStatus;

public class TechnicalError extends WanderException {

    public TechnicalError(String message, HttpStatus statusCode) {
        super(message, statusCode);
    }
}
