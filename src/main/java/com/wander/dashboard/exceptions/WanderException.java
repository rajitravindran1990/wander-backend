package com.wander.dashboard.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class WanderException extends Exception {
    private HttpStatus statusCode;

    WanderException(String message, HttpStatus statusCode) {
        super(message);
        this.statusCode = statusCode;
    }
}
