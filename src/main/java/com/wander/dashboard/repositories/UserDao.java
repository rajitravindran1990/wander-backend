package com.wander.dashboard.repositories;

import com.wander.dashboard.beans.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface UserDao extends CrudRepository<User, Integer> {

    User findByEmail(String email);
}
