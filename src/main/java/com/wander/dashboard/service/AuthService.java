package com.wander.dashboard.service;

import com.wander.dashboard.beans.User;
import com.wander.dashboard.beans.request.LoginRequest;
import com.wander.dashboard.beans.request.SignUpReq;
import com.wander.dashboard.exceptions.AuthException;
import com.wander.dashboard.exceptions.TechnicalError;
import com.wander.dashboard.exceptions.WanderException;
import com.wander.dashboard.repositories.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    UserDao userDao;

    public User signUp(SignUpReq req) throws WanderException {
        User user = null;
        try {
            user = userDao.save(getUser(req));
        } catch (Exception e) {
            if (e.getMessage().contains("[users_un]")) {
                throw new AuthException("User already exists!!", HttpStatus.CONFLICT);
            } else {
                throw new TechnicalError("Technical Error!! Please try after some time", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return user;
    }

    public User login(LoginRequest req) throws WanderException {
        User user = userDao.findByEmail(req.getEmail());
        if (user == null) {
            throw new AuthException("Email not registered !!", HttpStatus.UNAUTHORIZED);
        } else if (!user.getPassword().equals(req.getPassword())) {
            throw new AuthException("Wrong email or password !!", HttpStatus.UNAUTHORIZED);
        }
        return user;
    }

    private User getUser(SignUpReq req) {
        User u = new User();
        u.setEmail(req.getEmail());
        u.setName(req.getName());
        u.setPassword(req.getPassword());
        return u;
    }
}
