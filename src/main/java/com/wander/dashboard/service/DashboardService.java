package com.wander.dashboard.service;

import com.wander.dashboard.CovidData;
import com.wander.dashboard.beans.CovidDataHeader;
import com.wander.dashboard.beans.CovidDataRow;
import com.wander.dashboard.exceptions.WanderException;
import com.wander.dashboard.utils.Utils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class DashboardService {
    private static String url = "https://api.apify.com/v2/key-value-stores/tVaYRsPHLjNdNBu7S/records/LATEST?disableRedirect=true";

    public CovidData getCovidData() throws WanderException {
        RestTemplate restTemplate = new RestTemplate();
        CovidData data = new CovidData();
        ResponseEntity<List> response = restTemplate.getForEntity(url, List.class);
        List<CovidDataRow> rows = new LinkedList<CovidDataRow>();
        for(Object o : response.getBody()) {
            CovidDataRow r = new CovidDataRow();
            Map<String, Object> map = (Map)o;
            r.setCountry((String)map.get("country"));
            r.setTested(map.get("tested"));
            r.setInfected(map.get("infected"));
            r.setRecovered(map.get("recovered"));
            r.setDeceased(map.get("deceased"));
            r.setLastUpdatedSource(Utils.getDateString((String)map.get("lastUpdatedSource")));
            rows.add(r);
        }
        List<CovidDataHeader> headers = new LinkedList<>();
        headers.add(new CovidDataHeader("Country","country","asc"));
        headers.add(new CovidDataHeader("Tested","tested","asc"));
        headers.add(new CovidDataHeader("Infected","infected","asc"));
        headers.add(new CovidDataHeader("Recovered","recovered","asc"));
        headers.add(new CovidDataHeader("Deceased","deceased","asc"));
        headers.add(new CovidDataHeader("Last Updated","lastUpdatedSource","asc"));
        data.setColumns(headers);
        data.setRows(rows);
        return data;

    }

}
