package com.wander.dashboard.utils;

public class ResourceUrl {
    public static final String SIGN_UP = "/signup";
    public static final String LOGIN = "/login";
    public static final String DASHBOARD = "/dashboard";
}
