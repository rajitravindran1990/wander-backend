package com.wander.dashboard.utils;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss");

    public static boolean isValidEmail(String emailStr) {
        if (StringUtils.isBlank(emailStr)) {
            return false;
        }
        Pattern emailPattern = Pattern.compile("^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$");
        Matcher matcher = emailPattern.matcher(emailStr);
        return matcher.matches();
    }

    public static boolean isValidPassword(String password) {
        return StringUtils.isNotBlank(password) && password.length() >= 8;
    }

    public static boolean isValidHeader(String header) {
        return StringUtils.isNotEmpty(header) && header.split(" ").length == 2 && StringUtils.isNotEmpty(header.split(" ")[1]);
    }


    public static String getDateString(String dateStr) {
        String dateString = "";
        Calendar cal = Calendar.getInstance();
        try {
            Date date = dateFormat.parse(dateStr);
            int diff = (int) (((cal.getTimeInMillis() - date.getTime()) / (1000 * 60 * 60)));
            if (diff / 60 > 0) {
                int hrs = diff / 60;
                if (hrs / 24 > 0) {
                    int days = hrs / 24;
                    dateString = days + " days ago";
                } else {
                    dateString = hrs % 24 + " hours ago";
                }
            } else {
                dateString = (diff % 60) + " mins ago";
            }
        } catch (Exception e) {
            dateString = "NA";
        }
        return dateString;
    }

}